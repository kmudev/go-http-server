# go-http-server

Example HTTP server with logging, written in Go.

## Usage

Build with `docker build -t go_http_server .`\
Run with `docker run --rm -i -p 9000:9000 go_http_server`
