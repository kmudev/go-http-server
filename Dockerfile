# syntax=docker/dockerfile:1

FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git tzdata ca-certificates && update-ca-certificates

# Create user and choose high UID
ENV SERVICE_NAME="go_http_server"
ENV UID=10101

RUN addgroup --gid ${UID} -S ${SERVICE_NAME} && \
    adduser -G ${SERVICE_NAME} --shell /bin/false --disabled-password -H --uid ${UID} ${SERVICE_NAME} && \
    mkdir -p /var/run/${SERVICE_NAME} /var/log/${SERVICE_NAME} && \
    chown ${SERVICE_NAME}: /var/run/${SERVICE_NAME} /var/log/${SERVICE_NAME}

# Uncomment following lines to include "dumb-init".
#ARG DUMB_INIT_URL="https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64"
#ADD ${DUMB_INIT_URL} /usr/bin/dumb-init
#RUN chmod +x /usr/bin/dumb-init

WORKDIR $GOPATH/src/main/${SERVICE_NAME}/

COPY ./src/ .

RUN go mod init && go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/${SERVICE_NAME}

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /go/bin/${SERVICE_NAME} /go/bin/${SERVICE_NAME}
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /var/run/${SERVICE_NAME} /var/run/${SERVICE_NAME}
COPY --from=builder /var/log/${SERVICE_NAME} /var/log/${SERVICE_NAME}

USER ${SERVICE_NAME}:${SERVICE_NAME}
EXPOSE 9000

# Uncomment following line if using "dumb-init"
#ENTRYPOINT ["/usr/bin/dumb-init", "/go/bin/${SERVICE_NAME}"]

ENTRYPOINT ["/go/bin/go_http_server"]
