package main

import (
    "fmt"
    "log"
    "net/http"
)

func rootHandler(res http.ResponseWriter, req *http.Request) {
    header := res.Header()
    header.Set("Content-Type","application/json")
        
    res.WriteHeader(http.StatusOK)
    fmt.Fprint(res, `{ "status": "OK" }`)
}

func logReq(handler http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
            log.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
            handler.ServeHTTP(w, r)
    })
}

func main() {
    log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
    httpPort := ":9000"

    fmt.Printf("Go HTTP Server\n")
    fmt.Printf("Listening on port %v\n", httpPort)

    http.HandleFunc("/", rootHandler)
    
    err := http.ListenAndServe(httpPort, logReq(http.DefaultServeMux)) 
    if err != nil {
        log.Fatal(err)
    }   
}
